import json
import asyncio
import os
import aiohttp
# import time
# from bs4 import BeautifulSoup as bs
# import requests
# from aiofile import async_open

# Define the JSON file path
json_file = 'val_manifest.json.clean'


async def process_item(item, voice="alloy", model="tts-1-hd", speed="1.0"):
    # voice : alloy, echo, fable, onyx, nova, shimmer
    # model : tts-1, tts-1-hd
    # speed : 0.25-4 (step=0.25)

    # Define the request headers
    model = "tts-1-hd"
    voice = "alloy"
    speed = "1.0"

    # Define the request headers
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer YOUR_ACCESS_TOKEN_HERE',  # Replace with your access token
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) '
                      'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Mobile Safari/537.36',
        'Accept-Language': 'en-US,en;q=0.9,vi;q=0.8,es;q=0.7,de;q=0.6,zh-TW;q=0.5,zh;q=0.4,ru;q=0.3,uk;q=0.2',
        # Add other headers if needed
    }

    # Define the request payload (JSON data)
    payload_data = {
        "model": model,
        "input": item['text'],
        "voice": voice,
        "speed": speed
    }

    async with aiohttp.ClientSession() as session:
        # Make a POST request to the API
        async with session.post('https://api.ttsopenai.com/api/v1/audio/text-to-speech', headers=headers,
                                json=payload_data) as response:
            # Check if the request was successful (status code 200)
            if response.status == 200:
                # Assuming the response content is audio/mpeg
                audio_content = await response.read()
                # Extract filename from audio_filepath
                directory = os.path.dirname(item['audio_filepath']).split('/')[-1]
                os.makedirs(directory, exist_ok=True)  # Create directory if it doesn't exist
                # test = os.path.basename(item['audio_filepath']).split('.wav')[0]
                # print(f"filename {test}")
                filename = os.path.basename(item['audio_filepath']).split('.wav')[0] + '.wav'
                output_path = os.path.join(directory, filename)
                print(f"filename {filename} directory {directory}")
                # Save the audio content to a file
                with open(output_path, 'wb') as f:
                    f.write(audio_content)
                print(f'Audio saved successfully as: {output_path}')
            else:
                # Print an error message if the request was not successful
                print('Error:', response.status)


async def main():
    # Read the JSON file
    with open(json_file, encoding='utf-8') as f:
        data = [json.loads(line.strip()) for line in f]

    # Create a queue to hold the items
    queue = asyncio.Queue()

    # Fill the queue with items from the JSON data
    for item in data:
        await queue.put(item)

    # Define the number of concurrent tasks (adjust as needed)
    num_concurrent_tasks = 5

    # Create a list to hold the coroutines
    tasks = []

    # Create and add coroutines to the list
    for _ in range(num_concurrent_tasks):
        task = asyncio.create_task(worker(queue))
        tasks.append(task)

    # Wait for all tasks to complete
    await asyncio.gather(*tasks)


async def worker(queue):
    while not queue.empty():
        # Get an item from the queue
        item = await queue.get()
        # Process the item
        await process_item(item)


# Run the main coroutine
asyncio.run(main())
